function mse = enetFitAndPredict(Xtrain, Ytrain, Xtest, Ytest, lambda, alpha)

B = glmnet_enet(Xtrain, Ytrain, lambda, alpha);
[m, n]=size(Ytest);
mse = norm(Ytest-Xtest*B, 'fro')^2/(m*n);
