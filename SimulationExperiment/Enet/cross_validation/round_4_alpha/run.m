load filter_norm_expression;

matlabpool open;

%T is time length
%p is model order
%n is gene number
%m=T-p
p=3;

temp=size(expression);
n=temp(1);
T=temp(2);

m=T-p;

Y=expression(:,(p+1):T)';

X=zeros(m, n*p);

for i=1:m
    for j=1:p
        X(i,n*(j-1)+1:n*j)=expression(:,i+p-j)';
    end
end

save('workspace');

% lasso regression
% and cross validation 
wid=fopen('cvMSE_result', 'w');

lambda=0.1;
for alpha=[1,0.9,0.5,1e-1,1e-2,1e-3]
    cvMSE=cross_validation(X, Y, lambda, alpha, 5);
    fprintf(wid, '%.10f\t%.10f\t%.10f\n', alpha, mean(cvMSE), std(cvMSE));
    fclose(wid);
    wid=fopen('cvMSE_result', 'a');
end
fclose(wid);

matlabpool close;
exit;
