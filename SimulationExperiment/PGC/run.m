load filter_norm_expression;

timelength=size(expression,2);
n=size(expression,1);
filename='output';
wid=fopen(filename,'w');

for i=1:n
    for j=(i+1):n
        arraytwo=[i,j];
        modelorder=3;
        ret = cca_granger_regress(expression(arraytwo, :), modelorder);
        %if ret.prb(2,1)<0.01
        fprintf(wid, '%d\t%d\t%f\n', arraytwo(1), arraytwo(2),ret.prb(2,1));
        %end
        %if ret.prb(1,2)<0.01
        fprintf(wid, '%d\t%d\t%f\n', arraytwo(2), arraytwo(1),ret.prb(1,2));
        %end
    end
end

exit;
