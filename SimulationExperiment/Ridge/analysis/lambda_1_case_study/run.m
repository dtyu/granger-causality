load filter_norm_expression;

%T is time length
%p is model order
%n is gene number
%m=T-p
p=3;

temp=size(expression);
n=temp(1);
T=temp(2);

m=T-p;

Y=expression(:,(p+1):T)';

X=zeros(m, n*p);

for i=1:m
    for j=1:p
        X(i,n*(j-1)+1:n*j)=expression(:,i+p-j)';
    end
end


% Ridge regulation to calculate B matrix
B=self_ridge(X,Y,1);

save('workspace');
save('workspace_B', 'B');

exit;
