load filter_norm_expression;

%T is time length
%p is model order
%n is gene number
%m=T-p
p=3;

temp=size(expression);
n=temp(1);
T=temp(2);

m=T-p;

Y=expression(:,(p+1):T)';

X=zeros(m, n*p);

for i=1:m
    for j=1:p
        X(i,n*(j-1)+1:n*j)=expression(:,i+p-j)';
    end
end

save('workspace');

% Ridge regulation using different Lambda
% Cross validation applied to check model
wid=fopen('Result_cvMSE', 'w');
for lambda=[1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,0.01,0.1,1,10,100]
    cvMSE=cross_validation(X, Y, lambda, 5);
    fprintf(wid, '%.10f\t%.10f\t%.10f\n', lambda, mean(cvMSE), std(cvMSE));
    fclose(wid);
    wid=fopen('Result_cvMSE', 'a');
end
fclose(wid);

exit;
