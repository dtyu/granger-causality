% This file deals with generating the data using linear model. 

% Load the regulatory pairs from the golden standard. 
% These are the connected nodes in the regnet
n = 780;

load activated_part;
start = activated_part;
clear activated_part;

load pairs_golden_standard;
reg_pairs = pairs_golden_standard;
clear pairs_golden_standard;

acti=zeros(n,1);

% Activate the start nodes. 
for i=1:size(start, 1)
    acti(start(i))=1;
end

% We can not use for loop to do it. 
% Not True: the regulated pairs are all from lower to higher value. 
% But since we know the maximum travel length between nodes are 3. 
% Going through it three times are enough. 
for i=1:3
    for j=1:size(reg_pairs, 1)
        if acti(reg_pairs(j, 1))~=0
            acti(reg_pairs(j,2))=1;
        end
    end
end

dict=find(acti~=0);
dict=[[1:size(dict,1)]' dict];
dlmwrite('dict', dict, '\t');

exit;
