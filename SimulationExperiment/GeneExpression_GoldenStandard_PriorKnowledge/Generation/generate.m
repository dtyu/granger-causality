% Generate the simulation regulatory network using MATLAB. 
% Three layer structure. 
% Check the README in this folder. 

% Initiating the Adjancency Matrix
% 1110*1110 non-symmetric matrix 
% A(i, j) represents the causality from i->j. 

n=780;
A=zeros(n, n);

topLayer = 1:60;
middleLayer = 61:240;
bottomLayer = 241:780;

pairs_filename='pairs_golden_standard';
pairs_wid=fopen(pairs_filename, 'w');

self_filename='self_golden_standard';
self_wid=fopen(self_filename, 'w');

activate_filename='activated_part';
activate_wid=fopen(activate_filename, 'w');

% Generating the top to middle edges
for i=topLayer
    A(i, (i*3+58):(i*3+60))=1;
end

% Generating the middle to bottom edges
for i=middleLayer
    A(i, (i*3+58):(i*3+60))=1;
end

% Randomly perturb the regulatory network
% The percentage is 60 in 60*180 possible edges. 
for i=1:60
    temp1 = randi(60);
    temp2 = randi(180)+60;
    A(temp1, temp2) = 1;
end

% The percentage is 180 in 180*180 possible edges.  
for i=1:180
    temp1 = randi(180)+60;
    temp2 = randi(180)+60;
    A(temp1, temp2) = 1;
end

% The percentage is 540 in 540*180 possible edges
for i=1:540
    temp1 = randi(180)+60;
    temp2 = randi(540)+240;
    A(temp1, temp2) = 1;
end

% Print out self regularization including decay factor as a key parameter. 
array=sum(A);
for i=1:length(array)
    if array(i)==0
        % Half of the master nodes are not activated
        if rand()>0.5
            fprintf(self_wid, '%d\t%d\t%d\t%f\n', i, i, 1, 0.95+0.05*rand());
            fprintf(activate_wid, '%d\n', i);
        else
            fprintf(self_wid, '%d\t%d\t%d\t%f\n', i, i, 1, 0);
        end
    end
end

% Print out the Regulatory network
% The regulatory coefficient are generated from -1 to 1 by
% a uniform distribution
% The model order p is controlled by randi from 1 to 3
for i=1:n
    for j=1:n
        if i~=j && A(i, j)~=0
            fprintf(pairs_wid, '%d\t%d\t%d\t%f\n', i, j, randi(3), -1+2*rand()); 
        end
    end
end

fclose(pairs_wid);
fclose(self_wid);
exit;
