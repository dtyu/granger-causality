% This file deals with generating the data using linear model. 

% Load the regulatory pairs from the golden standard. 
n = 780;

load pairs_golden_standard;
pairs = pairs_golden_standard;
load self_golden_standard;
self = self_golden_standard;

regnum = size(pairs, 1);

% Generating the regulatory coefficients
% Not needed anymore due to 
% pairs(:,3)= -1 + 2.*rand(regnum, 1)

% Generating the initial few time points
T=20;
expression = zeros(n, T);

% Generating the expression values
% First 1:3 time points are random data
% Maximum model order considered here is 3

expression(1:n,1)=normrnd(0,1,n,1);
expression(1:n,2)=normrnd(0,1,n,1);
expression(1:n,3)=normrnd(0,1,n,1);

% Calculate autoregression coefficient
% 36 time points with 3 cycles are considered 
a = exp((1/6)*pi*1.0i);
b = exp(-(1/6)*pi*1.0i);
c = a+b;
d = a*b;

for i=4:T
    for j=1:n
        % Initialization put in the noise value
        expression(j,i) = normrnd(0, 1);

        % Calculating the expression value for "j"
        % Find the causes of j
        array = find(pairs(:,2)==j);

        % Here actually only the top layer does not have a regulator
        % They are dealt with using auto regulation
        if size(array, 1)==0
            index=find(self(:,2)==j);
            expression(j,i) = expression(j,i) + c*self(index, 4)*expression(j,i-1) - d * self(index,4) * self(index,4) * expression(j,i-2);
        else
            % The regulated part did not use auto-regulation
            % Using the random coefficient to generate the network
            for k=1:size(array,1)
                expression(j,i) = expression(j,i) + pairs(array(k),4)*expression(pairs(array(k),1), i-pairs(array(k), 3));
            end
        end
    end
end

% Adding 349 independent self regulatory periodic nodes.
for i=781:1129
    for j=1:3
        expression(i,j) = normrnd(0, 1);
    end
end

for i=781:1129
    for j=4:T
        %expression(i,j) = c * expression(i,j-1) - d * expression(i,j-2);
        expression(i,j) = c * expression(i,j-1) - d * expression(i,j-2) + normrnd(0, 1);
    end
end

save expression expression;
exit;
