function mse = lassoFitAndPredict(Xtrain, Ytrain, Xtest, Ytest,lambda)

B = glmnet_lasso(Xtrain, Ytrain,lambda);
[m, n]=size(Ytest);
mse = norm(Ytest-Xtest*B, 'fro')^2/(m*n);
