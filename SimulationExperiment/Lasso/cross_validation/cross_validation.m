function cvMSE = cross_validation(X, Y, lambda, K)
% We could not use default crossval with "mse" since Y is not a column vector.
% Y is a matrix, thus we have different measures.
% Use X

cvfun=@(Xtrain, Ytrain, Xtest, Ytest) lassoFitAndPredict(Xtrain, Ytrain, Xtest, Ytest, lambda);

%regf=@(Xtrain, Ytrain, Xtest) Xtest*self_ridge(Xtrain, Ytrain, lambda);
cvp=cvpartition(size(X, 1), 'Kfold', K);

%cvMSE = crossval(cvfun, 'partition', cvp);

cvMSE=crossval(cvfun, X, Y, 'Partition', cvp, 'Mcreps', 5 );

