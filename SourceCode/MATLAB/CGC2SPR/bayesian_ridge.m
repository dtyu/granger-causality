function [B,mse] = bayesian_lasso(X,Y,W,lambdaone,lambdatwo)
% X is a n by p matrix
% Y is a n by m matrix
% solve the problem of ||Y-XB||+lambdaone*(B-lambdatwo*W)^2

if( ~ismatrix(X) || length(size(X)) ~= 2 || ~isreal(X))
    error('X is not a 2D matrix');
end

[n,p]=size(X);

if( ~ismatrix(Y) || size(Y,1) ~= size(X,1))
    error('Y size is not matching X');
end
m=size(Y,2);


B=zeros(p, m);
B=self_ridge(X,Y,lambdaone);
W=W.*sign(B);

B=inv(X'*X+lambdaone*eye(p))*(X'*Y+lambdaone*lambdatwo*W);
mse = norm(Y-X*B, 'fro')/(m*n);
