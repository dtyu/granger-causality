load('ymc-periodic-std');
expression=ymc_periodic_std;

%matlabpool open;

%T is time length
%p is model order
%n is gene number
%m=T-p
p=2;

temp=size(expression);
n=temp(1);
T=temp(2);

m=T-p;

Y=expression(:,(p+1):T)';

X=zeros(m, n*p);

for i=1:m
    for j=1:p
        X(i,n*(j-1)+1:n*j)=expression(:,i+p-j)';
    end
end

load('TF_binding_score');
W=zeros(n*p, n);
for i=1:size(TF_binding_score, 1)
    for j=1:p
        W(TF_binding_score(i,1)+n*(j-1), TF_binding_score(i,2))=TF_binding_score(i,3);
    end
end

% lasso regression
B=bayesian_ridge(X,Y,W,1e-2,2e-6);

save('workspace');
save('workspace_B', 'B');
%matlabpool close;

exit;
