load workspace_B;

n=size(B,2);
p=size(B,1)/size(B,2);

B=abs(B);

A=zeros(n);
for i=1:p
    A=max(A, B(((i-1)*n+1):((i-1)*n+n) ,:));
end
save workspace_A A;

%filename='output';
%wid=fopen('output', 'w');
%for i=1:n
%    for j=(i+1):n
%        fprintf(wid, '%d\t%d\t%f\n', i,j,A(i,j));
%        fprintf(wid, '%d\t%d\t%f\n', j,i,A(j,i));
%    end
%end
%fclose(wid);
