load workspace_A;

load ymc_reg_indirect;

% This scripts construct a new array based on the regpairs

regpair=ymc_reg_indirect;

% calculating mdoel order and gene number

for i=1:size(regpair, 1)
    C(i)=A(regpair(i,1),regpair(i,2));
end

% Calculating size distribution of C

wid=fopen('Regnet_Size_distribution', 'w');

fprintf(wid, '<1e-8\t%d\n', size(find(C<1e-8),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-8, 1e-7, size(find(C>=1e-8),2)-size(find(C>=1e-7),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-7, 1e-6, size(find(C>=1e-7),2)-size(find(C>=1e-6),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-6, 1e-5, size(find(C>=1e-6),2)-size(find(C>=1e-5),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-5, 1e-4, size(find(C>=1e-5),2)-size(find(C>=1e-4),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-4, 1e-3, size(find(C>=1e-4),2)-size(find(C>=1e-3),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-3, 1e-2, size(find(C>=1e-3),2)-size(find(C>=1e-2),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-2, 1e-1, size(find(C>=1e-2),2)-size(find(C>=1e-1),2));
fprintf(wid, '%.10f-%.10f\t%d\n', 1e-1, 1, size(find(C>=0.1),2)-size(find(C>=1),2));
fprintf(wid, '>=1\t%d\n', size(find(C>=1),2));

fclose(wid);

exit;
