#!/usr/bin/perl -w
use strict;

if($#ARGV!=2)
{
	print("script usage: script dict input output\n");
	exit(1);
}

my $dict=$ARGV[0];
my $input=$ARGV[1];
my $output=$ARGV[2];

my @array;
my $line;

my $dicthash={};
open(DICT, $dict);
while($line=<DICT>)
{
	@array=split(' ', $line);
	if(exists $dicthash->{$array[2]})
	{
		next;
	}
	else
	{
		# this is a mixed hash
		$dicthash->{$array[2]}=$array[3];
		$dicthash->{$array[1]}=$array[3];
	}
}
close(DICT);

open(IN, $input);
open(OUT, ">".$output);

my $i;

while(my $line=<IN>)
{
	$line =~ s/\r//g;
	$line =~ s/"//g;
	$line =~ s/ //g;
	chomp($line);
	@array=split(' ', $line);
	$array[0]=uc($array[0]);
	if(exists $dicthash->{$array[0]})
	{
		if(exists $dicthash->{$array[1]})
		{
			print OUT $dicthash->{$array[0]}."\t".$dicthash->{$array[1]}."\t".$array[2]."\n";
		}
	}
}
close(IN);
close(OUT);
