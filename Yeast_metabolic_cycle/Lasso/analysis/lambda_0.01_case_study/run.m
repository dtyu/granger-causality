load('ymc-periodic-std');
expression=ymc_periodic_std;

matlabpool open;

%T is time length
%p is model order
%n is gene number
%m=T-p
p=2;

temp=size(expression);
n=temp(1);
T=temp(2);

m=T-p;

Y=expression(:,(p+1):T)';

X=zeros(m, n*p);

for i=1:m
    for j=1:p
        X(i,n*(j-1)+1:n*j)=expression(:,i+p-j)';
    end
end


% lasso regression
B=glmnet_lasso(X,Y,0.01);

save('workspace');
save('workspace_B', 'B');
matlabpool close;

exit;
