function B=self_nonorm_lasso(X,Y,lambda,alpha)

% Modified according to MATLAB lasso

% X a real 2D matrix
if ~ismatrix(X) || length(size(X)) ~= 2 || ~isreal(X)
    error(message('stats:lasso:XnotaReal2DMatrix'));
end

if size(X,1) < 2
    error(message('stats:lasso:TooFewObservations'));
end

% Y a vector, same length as the columns of X
if ~isvector(Y) || ~isreal(Y) || size(X,1) ~= length(Y)
    error(message('stats:lasso:YnotaConformingVector'));
end

% If Y is a row vector, convert to a column vector
if size(Y,1) == 1
    Y = Y';
end

reltol=1e-4;

% calculate lambdaMax and nullMSE
% the function is defined at the bottom of the file
%[lambdaMax, nullMSE] = computeLambdaMax(X,Y,alpha);

% Compiled from original lasso fit part

[N,P] = size(X);
% No standardization here
% Weights are not used as well
totalweight=N;

constantPredictors = (range(X)==0);
ever_active = ~constantPredictors;

b = zeros(P,1);
active = false(1,P);

lam = lambda;
%if lam >= lambdaMax
%    exit;
%end
threshold = lam * alpha;

% Denominator in coordinate descent update

% Choose the no standardization one and no weight one
shrinkFactor = (1/N) * ones(1,N)*(X.^2) + lam*(1 - alpha);

while true

    bold = b;

    [b,active] = cdescentCycle(X,X,Y, ...
        b,active,totalweight,shrinkFactor,threshold);

    if max( abs(b-bold)./max(1.0,min(abs(b),abs(bold))) ) < reltol
        % Cycling over the active set converged.
        % Do one full pass through the predictors.
        % If there is no predictor added to the active set, we're done.
        % Otherwise, resume the coordinate descent iterations.
        bold = b;
        potentially_active = thresholdScreen(X,X,Y,b,ever_active,threshold);
        if any(potentially_active)
            new_active = active | potentially_active;
            [b,new_active] = cdescentCycle(X,X,Y, ...
                b,new_active,totalweight,shrinkFactor,threshold);
        else
            new_active = active;
        end

        if isequal(new_active, active)
            break
        elseif max( abs(b-bold)./max(1.0,min(abs(b),abs(bold))) ) < reltol
            % We didn't change the coefficients enough by the full pass
            % through the predictors to warrant continuing the iterations.
            % The active coefficients after this pass and the
            % active coefficients prior to this pass differ.
            % This implies that a coefficient changed between zero
            % and a small numeric value.  There is no "fit-wise" reason
            % to prefer the before and after coefficients, so we
            % choose the more parsimonious alternative.
            if sum(new_active) > sum(active)
                b = bold;
            else
                active = new_active;
            end
            break
        else
            active = new_active;
        end
    end
end

B=b;

%function [lambdaMax, nullMSE] = computeLambdaMax(X,Y,alpha)
% X and Y are already almost centered in our case
% Just consider them as already normalized data

%[N,~]=size(X);
%dotp = abs(X' * Y);
%lambdaMax = max(dotp) / (N*alpha);
%nullMSE = mean(Y.^2);
%end

function [b,active] = cdescentCycle(X0, wX0, Y0, ...
    b, active, totalweight, shrinkFactor, threshold)
%
r = Y0 - X0(:,active)*b(active,:);

for j=find(active);
    bjold = b(j);

    % Regress j-th partial residuals on j-th predictor
    rj = r + b(j)*X0(:,j);
    bj = (wX0(:,j)'*rj) / totalweight;

    % Soft thresholding
    b(j) = sign(bj) .* max((abs(bj) - threshold), 0) ./ shrinkFactor(j);
    if b(j) == 0
        active(j) = false;
    end
    r = r - X0(:,j)*(b(j)-bjold);
end
end

function potentially_active = thresholdScreen(X0, wX0, Y0, ...
    b, active, threshold)
r = Y0 - X0(:,active)*b(active);
% We don't need the (b.*wX2)' term that one might expect, because it
% is zero for the inactive predictors.
potentially_active = abs(r' *wX0) > threshold;
end %-thresholdScreen

end
