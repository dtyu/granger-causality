function [B,mse] = glmnet_lasso(X,Y,lambda)
% X is a n by p matrix
% Y is a n by m matrix

if( ~ismatrix(X) || length(size(X)) ~= 2 || ~isreal(X))
    error('X is not a 2D matrix');
end

[n,p]=size(X);

if( ~ismatrix(Y) || size(Y,1) ~= size(X,1))
    error('Y size is not matching X');
end
m=size(Y,2);

B=zeros(p, m);
parfor i=1:m
    B(:,i)=self_nonorm_lasso(X,Y(:,i),lambda,1);
end

mse = norm(Y-X*B, 'fro')^2/(m*n);
