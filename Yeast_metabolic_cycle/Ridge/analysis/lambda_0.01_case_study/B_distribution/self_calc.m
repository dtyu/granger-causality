load('../workspace_B');

% B is a n*p by n matrix
% Where row represents the number of causes
% Column represents genes.

wid=fopen('Size_distribution', 'w');
B=abs(B);

fprintf(wid, '<1e-8\t%d\n', size(find(B<1e-8),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-8, 1e-7, size(find(B>=1e-8),1)-size(find(B>=1e-7),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-7, 1e-6, size(find(B>=1e-7),1)-size(find(B>=1e-6),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-6, 1e-5, size(find(B>=1e-6),1)-size(find(B>=1e-5),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-5, 1e-4, size(find(B>=1e-5),1)-size(find(B>=1e-4),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-4, 1e-3, size(find(B>=1e-4),1)-size(find(B>=1e-3),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-3, 1e-2, size(find(B>=1e-3),1)-size(find(B>=1e-2),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-2, 1e-1, size(find(B>=1e-2),1)-size(find(B>=1e-1),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-1, 1, size(find(B>=0.1),1)-size(find(B>=1),1));
fprintf(wid, '>=1\t%d\n', size(find(B>=1),1));

fclose(wid);

exit;
