load('../workspace_B')

load('./ymc_evidence_reg_pairs_sorted')

% This scripts construct a new array based on the regpairs

regpair=ymc_evidence_reg_pairs_sorted;

% calculating mdoel order and gene number
p=size(B,1)/size(B,2);
n=size(B,2);

for i=1:size(regpair, 1)
    for j=1:p
        C(i, j)=abs(B(n*(j-1)+regpair(i,1),regpair(i,2)));
    end
end

% Calculating size distribution of C

wid=fopen('evidence_Regnet_Size_distribution', 'w');

fprintf(wid, '<1e-8\t%d\n', size(find(C<1e-8),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-8, 1e-7, size(find(C>=1e-8),1)-size(find(C>=1e-7),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-7, 1e-6, size(find(C>=1e-7),1)-size(find(C>=1e-6),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-6, 1e-5, size(find(C>=1e-6),1)-size(find(C>=1e-5),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-5, 1e-4, size(find(C>=1e-5),1)-size(find(C>=1e-4),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-4, 1e-3, size(find(C>=1e-4),1)-size(find(C>=1e-3),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-3, 1e-2, size(find(C>=1e-3),1)-size(find(C>=1e-2),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-2, 1e-1, size(find(C>=1e-2),1)-size(find(C>=1e-1),1));
fprintf(wid, '%.10f\t%.10f\t%d\n', 1e-1, 1, size(find(C>=0.1),1)-size(find(C>=1),1));
fprintf(wid, '>=1\t%d\n', size(find(C>=1),1));

fclose(wid);

exit;
