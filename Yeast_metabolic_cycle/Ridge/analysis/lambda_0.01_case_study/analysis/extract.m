load workspace_A;

threshold=8.4817e-3;
filename='output';
wid=fopen(filename, 'w');

[row,column]=find(A>threshold);
for i=1:size(row)
    fprintf(wid, '%d\t%d\t%f\n', row(i), column(i), A(row(i), column(i)));
end
fclose(wid);
