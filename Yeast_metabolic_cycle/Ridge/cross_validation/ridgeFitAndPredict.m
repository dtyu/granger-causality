function mse = ridgeFitAndPredict(Xtrain, Ytrain, Xtest, Ytest,lambda)

B = self_ridge(Xtrain, Ytrain,lambda);
[m, n]=size(Ytest);
mse = norm(Ytest-Xtest*B, 'fro')^2/(m*n);
