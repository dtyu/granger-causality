#!/usr/bin/perl -w
use strict;

if($#ARGV!=2)
{
	print("script usage: script dict input output\n");
	exit(1);
}

my $dict=$ARGV[0];
my $input=$ARGV[1];
my $output=$ARGV[2];

my @array=();
my $line;

my $dicthash={};
open(DICT, $dict);
while($line=<DICT>)
{
	@array=split(' ', $line);
	if(exists $dicthash->{$array[2]})
	{
		next;
	}
	else
	{
		$dicthash->{$array[2]}=$array[3];
	}
}
close(DICT);

open(IN, $input);
open(OUT, ">".$output);

while(my $line=<IN>)
{
	@array=split(' ', $line);
	if( (exists $dicthash->{$array[0]}) && (exists $dicthash->{$array[1]}) )
	{
		print OUT ($dicthash->{$array[0]})."\t".($dicthash->{$array[1]})."\t".$array[2]."\n";
	}
}
close(IN);
close(OUT);
