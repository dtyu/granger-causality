#!/usr/bin/perl -w
use strict;

if($#ARGV!=2)
{
        print("script usage: script dict input output\n");
        exit(1);
}

my $dict=$ARGV[0];
my $input=$ARGV[1];
my $output=$ARGV[2];

my @array=();
my $line;
my ($i, $j);

# using dict hash to translate the TF binding score file

my $dicthash={};
open(DICT, $dict);
while($line=<DICT>)
{
        @array=split(' ', $line);
        if(exists $dicthash->{$array[2]})
        {
		next;
	}
	else
	{
		$dicthash->{$array[2]}=$array[3];
	}
        if(exists $dicthash->{$array[1]})
        {
		next;
	}
	else
	{
		$dicthash->{$array[1]}=$array[3];
	}
}
close(DICT);

open(IN, $input);
open(OUT, ">".$output);

# The first line is for name detecting
# 

$line=<IN>;
chomp($line);
my @TFarray=split(',', $line);
for($i=1;$i<=$#TFarray;$i++)
{
	$TFarray[$i]=uc($TFarray[$i]);
}
print "Size is ".($#TFarray+1)."\n";

while($line=<IN>)
{
	chomp($line);
	@array=split(',',$line);
	if(exists $dicthash->{$array[0]})
	{
		my $target=$dicthash->{$array[0]};
		for($i=1;$i<=$#TFarray;$i++)
		{
			if(exists $dicthash->{$TFarray[$i]})
			{
				my $source=$dicthash->{$TFarray[$i]};
				print OUT $source."\t".$target."\t".$array[$i]."\n";
			}
		}
	}
	else
	{
	}
}

close(IN);
close(OUT);
