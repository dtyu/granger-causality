library(ROCR);
curve <- read.table("./curve", header=F,sep="\t");

pred <- prediction(curve[,1], curve[,2]);
perf <- performance(pred, measure = "prec", x.measure = "tpr");
plot(perf);
