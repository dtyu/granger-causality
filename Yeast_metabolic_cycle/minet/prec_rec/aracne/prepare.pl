#!/usr/bin/perl -w
use strict;

if($#ARGV!=2)
{
    print "script usage: script goldenstd input output\n";
    exit(1);
}

my $gold=$ARGV[0];
my $input=$ARGV[1];
my $output=$ARGV[2];

my @arrayone;
my @arraytwo;
my @array;

my ($i,$j);

my $line;

open(GOLD, $gold);
my $count=0;
while($line=<GOLD>)
{
    @array=split(' ', $line);
    $arrayone[$count]=$array[0];
    $arraytwo[$count]=$array[1];
    $count++;
}
close(GOLD);

open(IN, $input);
open(OUT, '>'.$output);

my $label;

while($line=<IN>)
{
    $label=0;
    @array=split(' ', $line);
    for($i=0;$i<$count;$i++)
    {
        if($arrayone[$i]==$array[0] && $arraytwo[$i]==$array[1])
        {
            $label=1;
            last;
        }
    }
    print OUT $array[2]."\t".$label."\n";
}

close(IN);
close(OUT);
